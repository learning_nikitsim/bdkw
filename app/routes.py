from app import app
from flask import render_template, url_for, redirect, flash, session
from flask_login import login_user, logout_user, login_required, current_user
from app.forms import LoginForm, RegisterFirmForm, RegisterApplicantForm, AddSummaryForm, AddVacancyForm
from app.forms import ExecuteVacanciesForm, ExecuteSummaryForm
from app.models import User, Applicant, Firm, ResponceVacancy, ResponceSummary, RoleOfUser, Summary, Vacancy
from werkzeug.security import generate_password_hash, check_password_hash


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/login', methods=['GET', 'POST'])
def login():

    login_form = LoginForm()

    if login_form.submit.data:

        user = User.get_user_by_login(login_form.login.data)

        if user:

            if check_password_hash(user.password_of_user, login_form.password.data):

                login_user(user)
                session['role'] = user.id_of_role
                return redirect(url_for('index'))

            else:
                flash('Введён неверный пароль')
        else:
            flash('Пользователя с такими данными не существует')

    return render_template("login.html", login_form=login_form)


@app.route('/register_applicant', methods=['GET', 'POST'])
def register_applicant():

    register_form = RegisterApplicantForm()

    if register_form.submit.data:

        _hashed_password = generate_password_hash(register_form.password.data)

        User.add_user_applicant(register_form.login.data, _hashed_password)
        id_user = User.get_user_by_login(register_form.login.data)

        Applicant.add_applicant(id_user.id_of_user, register_form.fio.data, register_form.age.data)

        return redirect(url_for('login'))

    return render_template("registerapplicant.html", register_form=register_form)


@app.route('/register_firm', methods=['GET', 'POST'])
def register_firm():

    register_form = RegisterFirmForm()

    if register_form.submit.data:

        _hashed_password = generate_password_hash(register_form.password.data)

        User.add_user_firm(register_form.login.data, _hashed_password)
        id_user = User.get_user_by_login(register_form.login.data)

        Firm.add_firm(id_user.id_of_user, register_form.name.data)

        return redirect(url_for('login'))

    return render_template("registerfirm.html", register_form=register_form)


@app.route('/add_summary', methods=['GET', 'POST'])
@login_required
def add_summary():

    if session['role'] == 2:

        add_summary_form = AddSummaryForm()

        list_of_summaries = Summary.get_summary_for_applicant(current_user.get_id())

        if add_summary_form.submit.data:

            if add_summary_form.min_salary.data > add_summary_form.max_salary.data:
                flash('Минимальная зарплата не может быть больше максимальной')
                return redirect(url_for('add_summary'))

            Summary.add_summary(current_user.get_id(), add_summary_form.position.data,
                                add_summary_form.max_salary.data, add_summary_form.min_salary.data)
            return redirect(url_for('add_summary'))

        return render_template("addsummary.html", add_summary_form=add_summary_form, summaries=list_of_summaries)

    elif session['role'] != 2:
        return redirect(url_for('index'))


@app.route('/add_vacancy', methods=['GET', 'POST'])
@login_required
def add_vacancy():
    if session['role'] == 1:

        list_of_vacancies = Vacancy.get_vacancy_for_firm(current_user.get_id())

        add_vacancy_form = AddVacancyForm()

        if add_vacancy_form.submit.data:

            Vacancy.add_vacancy(current_user.get_id(), add_vacancy_form.position.data,
                                add_vacancy_form.max_salary.data, add_vacancy_form.min_salary.data)
            return redirect(url_for('add_vacancy'))

        return render_template("add_vacancy.html", add_vacancy_form=add_vacancy_form, vacancies=list_of_vacancies)

    elif session['role'] != 1:
        return redirect(url_for('index'))


@app.route('/vacancies', methods=['GET', 'POST'])
@login_required
def vacancies():
    if session['role'] == 2:

        execute_vacancies_form = ExecuteVacanciesForm()

        all_vacancies = Vacancy.get_list_of_vacancies()

        if execute_vacancies_form.submit.data:

            responce = ResponceVacancy.get_responce_vacancy(execute_vacancies_form.id_vacancy.data,
                                                 current_user.get_id())

            if responce:
                flash('Вы уже откликались на эту вакансию')
                return redirect(url_for('vacancies'))
            elif not responce:

                ResponceVacancy.execute_vacancy(current_user.get_id(),
                                                execute_vacancies_form.id_vacancy.data)
                return redirect(url_for('vacancies'))

        return render_template("vacancies.html", all_vacancies=all_vacancies,
                               execute_vacancies_form=execute_vacancies_form)
    elif session['role'] != 2:
        return redirect(url_for('index'))


@app.route('/summaries', methods=['GET', 'POST'])
@login_required
def summaries():
    if session['role'] == 1:

        execute_summary_form = ExecuteSummaryForm()

        all_summaries = Summary.get_list_of_summaries()

        if execute_summary_form.submit.data:

            responce = ResponceSummary.get_responce_summary(execute_summary_form.id_summary.data,
                                                            current_user.get_id())

            if responce:
                flash('Вы уже откликались на это резюме')
                return redirect(url_for('summaries'))

            elif not responce:
                ResponceSummary.execute_summary(current_user.get_id(),
                                                execute_summary_form.id_summary.data)
                flash('Вы успешно откликнулись на вакансию')
                return redirect(url_for('summaries'))

        return render_template("summaries.html", execute_summary_form=execute_summary_form, all_summaries=all_summaries)
    elif session['role'] != 1:
        return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    session.clear()
    return redirect(url_for('index'))
