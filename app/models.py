from flask_login import UserMixin
from app import db
from app import login_manager
from flask import flash, redirect, url_for


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    __tablename__ = '_user'
    __table_args__ = {'extend_existing': True}

    def get_id(self):
        return self.id_of_user

    @staticmethod
    def get_user_by_login(login):
        try:
            query = db.session.query(User)
            query = query.join(RoleOfUser)
            query = query.filter(User.login_of_user == login).first()
            return query
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('login'))

    @staticmethod
    def add_user_applicant(login, password):
        user_to_add = User(login_of_user=login, password_of_user=password, id_of_role=2)
        try:
            db.session.add(user_to_add)
            db.session.commit()
            flash('Пользователь успешно добавлен')
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('register_applicant'))

    @staticmethod
    def add_user_firm(login, password):
        user_to_add = User(login_of_user=login, password_of_user=password, id_of_role=1)
        try:
            db.session.add(user_to_add)
            db.session.commit()
            flash('Пользователь успешно добавлен')
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('register_firm'))


class Applicant(db.Model):
    __tablename__ = 'applicant'
    __table_args__ = {'extend_existing': True}

    @staticmethod
    def add_applicant(id_applicant, fio, age):
        applicant_to_add = Applicant(id_of_applicant=id_applicant, fio_of_applicant=fio, age_of_applicant=age)
        try:
            db.session.add(applicant_to_add)
            db.session.commit()
            flash('Пользователь успешно добавлен')
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('register_applicant'))


class Firm(db.Model):
    __tablename__ = 'firm'
    __table_args__ = {'extend_existing': True}

    @staticmethod
    def add_firm(id_firm, firm):
        firm_to_add = Firm(id_of_firm=id_firm, name_of_firm=firm)
        try:
            db.session.add(firm_to_add)
            db.session.commit()
            flash('Пользователь успешно добавлен')
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('register_firm'))


class ResponceVacancy(db.Model):
    __tablename__ = 'responce_vacancy'
    __table_args__ = {'extend_existing': True}

    @staticmethod
    def execute_vacancy(id_applicant, id_vacancy):
        vacancy_to_execute = ResponceVacancy(id_of_applicant=id_applicant, id_of_vacancy=id_vacancy)
        try:
            db.session.add(vacancy_to_execute)
            db.session.commit()
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('vacancies'))

    @staticmethod
    def get_responce_vacancy(id_vacancy, id_applicant):
        try:
            query = db.session.query(ResponceVacancy)
            query = query.filter(ResponceVacancy.id_of_applicant == id_applicant)
            query = query.filter(ResponceVacancy.id_of_vacancy == id_vacancy)
            return query.first()
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('vacancies'))


class ResponceSummary(db.Model):
    __tablename__ = 'responce_summary'
    __table_args__ = {'extend_existing': True}

    @staticmethod
    def execute_summary(id_firm, id_summary):
        summary_to_execute = ResponceSummary(id_of_firm=id_firm, id_of_summary=id_summary)
        try:
            db.session.add(summary_to_execute)
            db.session.commit()
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('summaries'))

    @staticmethod
    def get_responce_summary(id_summary, id_firm):
        try:
            query = db.session.query(ResponceSummary)
            query = query.filter(ResponceSummary.id_of_firm == id_firm)
            query = query.filter(ResponceSummary.id_of_summary == id_summary)
            return query.first()
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('summaries'))


class RoleOfUser(db.Model):
    __tablename__ = 'role_of_user'
    __table_args__ = {'extend_existing': True}


class Summary(db.Model):
    __tablename__ = 'summary'
    __table_args__ = {'extend_existing': True}

    @staticmethod
    def add_summary(id_applicant, position, max_salary, min_salary):
        summary_to_add = Summary(id_of_applicant=id_applicant, position_summary=position, min_salary=min_salary,
                                 max_salary=max_salary)
        try:
            db.session.add(summary_to_add)
            db.session.commit()
            flash('Резюме добавлено успешно')
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('add_summary'))

    @staticmethod
    def get_summary_for_applicant(id_applicant):
        try:
            query = db.session.query(Summary).filter(Summary.id_of_applicant == id_applicant).all()
            return query
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('add_summary'))

    @staticmethod
    def get_list_of_summaries():
        try:
            query = db.session.query(Summary, Applicant)
            query = query.join(Applicant)
            return query.all()
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('summaries'))


class Vacancy(db.Model):
    __tablename__ = 'vacancy'
    __table_args__ = {'extend_existing': True}

    @staticmethod
    def add_vacancy(id_firm, position, max_salary, min_salary):
        vacancy_to_add = Vacancy(id_of_firm=id_firm, position_vacancy=position, min_salary=min_salary,
                                 max_salary=max_salary)
        try:
            db.session.add(vacancy_to_add)
            db.session.commit()
            flash('Вакансия добавлена успешно')
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('add_vacancy'))

    @staticmethod
    def get_vacancy_for_firm(id_firm):
        try:
            query = db.session.query(Vacancy).filter(Vacancy.id_of_firm == id_firm).all()
            return query
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('add_vacancy'))

    @staticmethod
    def get_list_of_vacancies():
        try:
            query = db.session.query(Vacancy, Firm)
            query = query.join(Firm)
            return query.all()
        except:
            flash('Ошибка взаимодействия с базой данных! Попробуйте позже!')
            return redirect(url_for('vacancies'))
