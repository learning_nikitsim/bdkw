from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, SubmitField, HiddenField, IntegerField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    login = StringField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])
    submit = SubmitField("Авторизуйтесь")


class RegisterFirmForm(FlaskForm):
    login = StringField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])
    name = StringField(validators=[DataRequired()])
    submit = SubmitField("Зарегистрируйтесь!")


class RegisterApplicantForm(FlaskForm):
    login = StringField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])
    fio = StringField(validators=[DataRequired()])
    age = IntegerField(validators=[DataRequired()])
    submit = SubmitField("Зарегистрируйтесь!")


class AddSummaryForm(FlaskForm):
    position = StringField(validators=[DataRequired()])
    min_salary = IntegerField()
    max_salary = IntegerField()
    submit = SubmitField("Составить резюме")


class AddVacancyForm(FlaskForm):
    position = StringField(validators=[DataRequired()])
    min_salary = IntegerField()
    max_salary = IntegerField()
    submit = SubmitField("Оставить вакансию")


class ExecuteVacanciesForm(FlaskForm):
    id_vacancy = HiddenField()
    submit = SubmitField("Откликнуться на резюме")


class ExecuteSummaryForm(FlaskForm):
    id_summary = HiddenField()
    submit = SubmitField("Откликнуться на вакансию")
